/*
p4.c


Maxime POULAIN Dec 2018
PUISSANCE 4


*/




#include "p4.h"


void dispMenu(void){

	system("clear");


	printf("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓\n");
	printf("┃                                         ┃\n");
	printf("┃                Connect 4                ┃\n");
	printf("┃                                         ┃\n");
	printf("┃   1   Play game                         ┃\n");
	printf("┃   2   Set Name                          ┃\n");
	printf("┃   3   Set Size                          ┃\n");
	printf("┃   4   Instruction                       ┃\n");
	printf("┃   5   Credit                            ┃\n");
	printf("┃                                         ┃\n");
	printf("┃                                         ┃\n");
	printf("┃   9   Exit                              ┃\n");
	printf("┃                                         ┃\n");
	printf("┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛\n");
	printf("\n");


//┏┓┃┗┛━

}

int choixMenu(void){

	int choix;

	dispMenu();
	printf("Choice : ");
	scanf("%d",&choix);
	printf("\n");

	while((choix<1||choix>5)&&choix!=9){
		printf("Error\n");

		dispMenu();
		printf("Choice : ");
		scanf("%d",&choix);
		printf("\n");

	}


	return choix;

}







Cub* initTab(int n,int l){

	Cub *tab;
	int i,j;
	tab=(Cub*)malloc((n+1)*(l+1)*sizeof(Cub));
	if(tab==NULL){
		printf("Error malloc\n");
		return NULL;
	}



	for(i=0;i<n;i++){
		for(j=0;j<l;j++){
			tab[i*l+j]=NONE;
		}
		
	}

	return tab;


}


void dispTab(Cub* tab,int n,int l,int tour, char player1[],char player2[]){

	int i,j;


	dispPlayer(tour,player1,player2);


	//printf("╔");
	printf("┏");

	for(i=0;i<l;i++){
		//printf("═");
		printf("━━");
	}
	//printf("╗\n");
	printf("┓\n");


	for(i=0;i<n;i++){
		//printf("║");
		printf("┃");
		for(j=0;j<l;j++){

			if(tab[i*l+j]==NONE){
				printf("  " );
			}
			else if(tab[i*l+j]==PLYA){
				printf(RED "██" RESET);

			}
			else if(tab[i*l+j]==PLYB){
				printf(YEL "██" RESET);

			}

		}
		//printf("║\n");
		printf("┃\n");

	}


	//printf("╚");
	printf("┗");

	for(i=0;i<l;i++){
		//printf("═");
		printf("━━");
	}
	//printf("╝\n");
	printf("┛\n");


}




void engine(Cub *tab,int n,int l,int tour, char player1[],char player2[]){

	int i,j;


	for(i=0;i<n;i++){
		usleep(90000);
		for(j=0;j<l;j++){
			//printf("No");
			//printf("Condition i : %d\tj : %d\t\n", i,j);
			if(tab[i*l+j]!=NONE){

				
				system("clear");
				dispTab(tab,n,l,tour,player1,player2);


				if((tab[(i+1)*l+j]==NONE&&((i+1)*l<n*l))){
					tab[(i+1)*l+j]=tab[i*l+j];
					tab[i*l+j]=NONE;
				}
			}
		}
		//printf("\n");
	}



}




Cub checkWin(Cub *tab,int n,int l){
	int i,j;

	for(i=0;i<n;i++){
		for(j=0;j<l;j++){

			if(tab[i*l+j]!=NONE){
//verticale *4
				if(tab[i*l+j]==tab[(i+1)*l+j]&&tab[i*l+j]==tab[(i+2)*l+j]&&tab[i*l+j]==tab[(i+3)*l+j]){
					return tab[i*l+j];
				}
//horizontale *4
				if(tab[i*l+j]==tab[i*l+j+1]&&tab[i*l+j]==tab[i*l+j+2]&&tab[i*l+j]==tab[i*l+j+3]){
					return tab[i*l+j];
				}
//diagonale droite *4
				if(tab[i*l+j]==tab[(i+1)*l+j+1]&&tab[i*l+j]==tab[(i+2)*l+j+2]&&tab[i*l+j]==tab[(i+3)*l+j+3]){
					return tab[i*l+j];
				}
//diagonale gauche *4
				if(tab[i*l+j]==tab[(i+1)*l+j-1]&&tab[i*l+j]==tab[(i+2)*l+j-2]&&tab[i*l+j]==tab[(i+3)*l+j-3]){
					return tab[i*l+j];
				}

			}


		}
	}


	return NONE;

}




void game(Cub *tab,int n,int l,char player1[],char player2[]){



	Cub win;
	int option,player=0;


	system("clear");

	dispTab(tab,n,l,player,player1,player2);

	while(scanf("%d",&option)==0){
		printf("Error\n");
		scanf("%*c");
	}


	while(option!=-1){
		player=(player+1)%2;

		if(player==0){
			tab[((option)%(l+1))-1]=PLYA;
			
		}
		else{
			tab[((option)%(l+1))-1]=PLYB;
			
		}



		engine(tab,n,l,player,player1,player2);


		win=checkWin(tab,n,l);

//player1 et player2 inv ??


		if(win!=NONE){
			if(win==PLYA){
				printf("%s won the game ! \n",player2);
				usleep(1000000);
				break;
			}
			if(win==PLYB){
				printf("%s won the game ! \n",player1);
				usleep(1000000);
				break;
			}
		}


		while(scanf("%d",&option)==0){
			printf("Error\n");
			scanf("%*c");
		}

	}




}


void chooseName(char player1[],char player2[]){

	printf("\n\nSet names \n\n");
	scanf("%*c");
	printf("Set name for Player 1 : ");
	fgets(player1,40,stdin);
	player1[strlen(player1)-1]='\0';
	printf("\n\n");
	printf("Set name for Player 2 : ");
	fgets(player2,40,stdin);
	player2[strlen(player2)-1]='\0';
	printf("\n");




}



void chooseSize(int *n,int *l){

	int nAux,lAux;


	printf("\n\nGame Size\n");
	printf("Width : ");
	scanf("%d",&lAux);
	while(lAux<4){
		printf("Size too small\n");
		printf("Width : ");
		scanf("%d",&lAux);
	}

	printf("\n\n");
	printf("Height : ");
	scanf("%d",&nAux);
	while(nAux<2){
		printf("Size too small\n");
		printf("Height : ");
		scanf("%d",&nAux);
	}
	printf("\n\n");
	*n=nAux;
	*l=lAux;


}



void dispPlayer(int tour,char player1[],char player2[]){


	if(tour==0){
		printf(YEL "%s" RESET "\t%s\n",player1,player2);
	}

	if(tour==1){
		printf("%s" RED "\t%s\n" RESET ,player1,player2);
	}



}


void instruction(void){
	char c='o';

	while(1){

		system("clear");

		printf("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓\n");
		printf("┃                                         ┃\n");
		printf("┃                Connect 4                ┃\n");
		printf("┃                                         ┃\n");
		printf("┃   Place you pawn with digit number      ┃\n");
		printf("┃   Avoid putting char inside stdin       ┃\n");
		printf("┃   Random malloc error can occur         ┃\n");
		printf("┃                                         ┃\n");
		printf("┃                                         ┃\n");
		printf("┃   9   Exit                              ┃\n");
		printf("┃                                         ┃\n");
		printf("┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛\n");
		printf("\n");

		scanf("%c%*c",&c);
		if(c=='9'){
			return;
		}

		
	}

}


void credit(void){
	char c='o';

	for(int i=0;i<10;i++){

		system("clear");

		printf("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓\n");
		printf("┃                                         ┃\n");
		printf("┃                Connect 4                ┃\n");
		printf("┃                                         ┃\n");
		printf("┃                                         ┃\n");
		printf("┃              Maxime Poulain             ┃\n");
		printf("┃                                         ┃\n");
		printf("┃                                         ┃\n");
		printf("┃                                         ┃\n");
		printf("┃   9   Exit                              ┃\n");
		printf("┃                                         ┃\n");
		printf("┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛\n");
		printf("\n");


		usleep(100000);
		system("clear");

		printf("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓\n");
		printf("┃                                         ┃\n");
		printf("┃                Connect 4                ┃\n");
		printf("┃                                         ┃\n");
		printf("┃                                         ┃\n");
		printf("┃ " RED "             Maxime Poulain       " RESET "      ┃\n");
		printf("┃                                         ┃\n");
		printf("┃                                         ┃\n");
		printf("┃                                         ┃\n");
		printf("┃   9   Exit                              ┃\n");
		printf("┃                                         ┃\n");
		printf("┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛\n");
		printf("\n");
		usleep(100000);



		
	}
	scanf("%c%*c",&c);
	if(c=='9'){
		return;
	}

}