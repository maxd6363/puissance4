/*
p4.h


Maxime POULAIN Dec 2018
PUISSANCE 4


*/



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>



#define RED     "\x1b[31m"
#define GRE		"\x1b[32m"
#define YEL		"\x1b[33m"
#define BLU	    "\x1b[34m"
#define MAG		"\x1b[35m"
#define CYA   	"\x1b[36m"
#define RESET   "\x1b[0m"




typedef enum{NONE,PLYA,PLYB}Cub;







//Fonctions


Cub* initTab(int n,int l);
void dispTab(Cub* tab,int n,int l,int tour, char player1[],char player2[]);
void engine(Cub *tab,int n,int l,int tour, char player1[],char player2[]);
Cub checkWin(Cub *tab,int n,int l);
void chooseName(char player1[],char player2[]);
void chooseSize(int *n,int *l);
void dispPlayer(int tour,char player1[],char player2[]);


void game(Cub *tab,int n,int l,char player1[],char player2[]);

//Menu
void dispMenu(void);
int choixMenu(void);
void instruction(void);
void credit(void);











